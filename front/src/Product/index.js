import React from "react";
import Card from "react-bootstrap/Card";
// import Button from "react-bootstrap/Button";
import { Link } from "react-router-dom";

export default class Product extends React.Component {
  render() {
    return (
      <Card className="p-4">
        <Link to={"/product/" + this.props.product.id}>
          <Card.Img
            variant="top"
            src={
              this.props.product.picture ||
              "https://cdn.shopify.com/s/files/1/0484/7648/7837/products/outdoor-3681924_1920_1024x1024@2x.jpg?v=1600519245"
            }
          />
          <Card.Body>
            <Card.Title>{this.props.product.name}</Card.Title>
            <Card.Text>{this.props.product.price || "14"}€</Card.Text>
            {/* <Card.Footer>{this.props.product.price || "14"}€</Card.Footer> */}
            {/* <Button variant="primary">Go somewhere</Button> */}
          </Card.Body>
        </Link>
      </Card>
    );
  }
}
