import React from "react";
import { connect } from "react-redux";
import Product from '../Product'
import { addProducts } from "../store/actions/products";
import { setId, setName, addJwt } from "../store/actions/user";

import CardColumns from 'react-bootstrap/CardColumns'

import jwt from "jsonwebtoken";
import axios from 'axios'

import './style.css'

class ProductList extends React.Component {
    constructor() {
        super()
        this.state = {
            products: []
        }
    }

    componentWillMount() {
        let token = localStorage.getItem("token")
        console.log(token)
        if (!token) {
            this.props.history.push("/signIn");
        } else {
            let verify = jwt.verify(token, "123456789");

            this.props.addJwt(token);
            this.props.setId(verify.id);
            console.log(verify.id);
            console.log(this.props.id);
            this.props.setName(verify.name);
            axios
              .get("http://localhost:3000/product/", {
                headers: { authorization: `Bearer ${token}` },
              })
              .then((response) => {
                this.setState({ products: response.data });
                this.props.addProducts(response.data)
                console.log(response.data);
                console.log(this.props.id);
              })
              .catch((error) => {
                console.log(error);
              });
        }
    }
  render() {
    return (
      <CardColumns className="ProductList">
        {/* {this.props.jwt} */}
        {this.props.products
          ? this.props.products.map((elem) => {
              return <Product product={elem} key={elem.id}></Product>;
            })
          : ""}
      </CardColumns>
    );
  }
}

const mapStateToProps = (state) => ({
  jwt: state.user.jwt,
  products: state.product.products,
  id: state.user.id
});

const mapDispatchToProps = {
  addProducts,
  setId,
  setName,
  addJwt
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductList);
