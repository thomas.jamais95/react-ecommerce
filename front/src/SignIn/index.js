import React from 'react'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import Alert from "react-bootstrap/Alert"
import axios from 'axios'

import { connect } from "react-redux";
import { addJwt } from "../store/actions/user";
import { setId, setName, setPicture } from "../store/actions/user";

import jwt from "jsonwebtoken";

import './style.css'



class SignIn extends React.Component {

  constructor({dispatch}) {
    super();
    this.state = {
      showError: false,
      responseMsg: "",
    };
  }

  trySubmit = (event) => {
    event.preventDefault();
    let toSend = {
      email: event.target.elements.email.value,
      password: event.target.elements.password.value,
    };
    axios
      .post("http://localhost:3000/user/sign-in", toSend)
      .then((response) => {
        if (response.status === 203) {
          this.setState({ responseMsg: response.data, showError: true });
        } else {
            console.log(response)
            localStorage.setItem("token", response.data.token)
            console.log(localStorage.getItem("token"))
            let verify = jwt.verify(response.data.token, "123456789");


            this.props.addJwt(response.data.token)
            this.props.setId(verify.id)
            this.props.setName(verify.name);
            this.props.setPicture(verify.picture)
            // console.log(this.props)
            this.props.history.push("/");

        //   this.setState({ responseMsg: response.data, showSuccess: true });
        }
      })
      .catch((error) => {
          console.error(error)
        // this.setState({ responseMsg: error, showError: true });
      });
  };

  render() {
    return (
      <div className="signin-form-container">
        <Form
          className="align-items-center signin-form"
          width="50"
          onSubmit={this.trySubmit.bind(this)}
        >
          {this.state.showError ? (
            <Alert variant="danger">{this.state.responseMsg}</Alert>
          ) : (
            ""
          )}
          <Form.Group controlId="email">
            <Form.Label>Email address</Form.Label>
            <Form.Control required type="email" placeholder="Enter email" />
            <Form.Text className="text-muted">
              We'll never share your email with anyone else.
            </Form.Text>
          </Form.Group>

          <Form.Group controlId="password">
            <Form.Label>Password</Form.Label>
            <Form.Control required type="password" placeholder="Password" />
          </Form.Group>
          <Button variant="success" type="submit">
            Submit
          </Button>
        </Form>
      </div>
    );
  }
}

const mapDispatchToProps = {
  addJwt,
  setId,
  setName,
  setPicture
};

// const mapStateToProps = (state) => ({
//   jwt: state.user,
// });

export default connect(null, mapDispatchToProps)(SignIn);
