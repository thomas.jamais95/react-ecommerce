import React from "react";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
// import Form from "react-bootstrap/Form";
// import FormControl from "react-bootstrap/FormControl";
import Button from "react-bootstrap/Button";
import { Link, withRouter } from "react-router-dom";
import { connect } from "react-redux";

import { removeJwt, setId, setName } from "../store/actions/user";
import { addProducts } from "../store/actions/products";

import "./style.css";

class Header extends React.Component {

  removeToken = () => {
    this.props.removeJwt();
    this.props.setId(null)
    this.props.setName(null);
    this.props.addProducts(null);
    localStorage.removeItem("token");
    this.props.history.push("/signIn");
  };

  render() {
    // const location = useLocation();
    // const token = localStorage.getItem("token");

    let _this = this

    return (
      <Navbar bg="dark" variant="dark">
        <Navbar.Brand href="#home">HamacAttitude</Navbar.Brand>
        <Nav className="mr-auto">
          {this.props.jwt && (
            <Nav.Item>
              <Nav.Link as={Link} to="/">
                Products
              </Nav.Link>
            </Nav.Item>
          )}
          {this.props.jwt && (
            <Nav.Item>
              <Nav.Link as={Link} to="/addProduct">
                Add Product
              </Nav.Link>
            </Nav.Item>
          )}
          {!this.props.jwt && (
            <Nav.Item>
              <Nav.Link as={Link} to="/signIn">
                SignIn
              </Nav.Link>
            </Nav.Item>
          )}
          {!this.props.jwt && (

          <Nav.Item>
            <Nav.Link as={Link} to="/signUp">
              SignUp
            </Nav.Link>
          </Nav.Item>
            )}
        </Nav>
        {/* {this.props.jwt && (
          <Form inline>
            <FormControl type="text" placeholder="Search" className="mr-sm-2" />
            <Button variant="outline-info">Search</Button>
          </Form>
        )} */}
        {this.props.jwt && (
          <img
            src={this.props.picture}
            width="50px"
            alt={this.props.name}
            style={{ borderRadius: "50%" }}
          />
        )}
        {this.props.jwt && (
          <Button
            id="signout"
            variant="outline-danger"
            onClick={this.removeToken.bind(_this)}
          >
            SignOut
          </Button>
        )}
      </Navbar>
    );
  }
}

const mapDispatchToProps = {
  removeJwt,
  addProducts,
  setName,
  setId
};

const mapStateToProps = (state) => ({
  jwt: state.user.jwt,
  picture: state.user.picture,
  name: state.user.name
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Header));
