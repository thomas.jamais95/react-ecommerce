import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './App';
// import Home from './Home/index';
import Header from './Header/index';
import SignIn from "./SignIn/index";
import SignUp from "./SignUp/index";
import ProductList from "./ProductList";
import AddProduct from "./AddProduct";
import DisplayProduct from "./DisplayProduct";
import * as serviceWorker from './serviceWorker';
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";

import { Provider } from 'react-redux'
import { createStore } from "redux";
import rootReducer from "./store/index";
import { persistStore, persistReducer } from "redux-persist"
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web
import { PersistGate } from "redux-persist/integration/react";
import "bootstrap/dist/css/bootstrap.min.css";



const persistConfig = {
  key: "root",
  storage,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = createStore(persistedReducer);
const persistore = persistStore(store)


const routing = (
  <Provider store={store}>
    <PersistGate persistor={persistore}>
      <Router>
        <Header />
        <Switch>
          <Route exact path="/" component={ProductList} />
          <Route path="/addProduct" component={AddProduct} />
          <Route path="/signIn" component={SignIn} />
          <Route path="/signUp" component={SignUp} />
          <Route path="/product/:id" component={DisplayProduct}/>
        </Switch>
      </Router>
    </PersistGate>
  </Provider>
);


ReactDOM.render(
  routing,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
