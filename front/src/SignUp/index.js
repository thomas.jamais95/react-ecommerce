import React from 'react'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import Col from 'react-bootstrap/Col'
import Alert from 'react-bootstrap/Alert'
import axios from 'axios'

import './style.css'

export default class SignUp extends React.Component {

    constructor() {
      super()
      this.state = {
        errorPass: false,
        showError: false,
        showSuccess: false,
        responseMsg: ''
      }
    }

    trySubmit = (event) => {
      event.preventDefault()
      if (event.target.elements.password.value !== event.target.elements.confirmPassword.value) {
        this.setState({errorPass: true})
        // this.state.errorPass = true
      } else {
        this.setState({errorPass: false});

        // this.state.errorPass = false
        let toSend = {
          firstName: event.target.elements.firstName.value,
          lastName: event.target.elements.lastName.value,
          email: event.target.elements.email.value,
          password: event.target.elements.password.value,
          picture: event.target.elements.picture.value,
        };
        axios.post("http://localhost:3000/user/sign-up", toSend).then(response => {
          if (response.status === 203) {
            this.setState({responseMsg: response.data, showError: true})
          } else {
            this.setState({ responseMsg: response.data, showSuccess: true });
          }
          console.log(response)
        }).catch(error => {
          console.log(error)
        })
        console.log(toSend)        
      }
    }

    render() {
        return (
          <div className="signup-form-container">
            <Form
              className="align-items-center signup-form"
              width="50"
              onSubmit={this.trySubmit.bind(this)}
            >
              {this.state.showError ? (
                <Alert variant="danger">{this.state.responseMsg}</Alert>
              ) : (
                ""
              )}
              {this.state.showSuccess ? (
                <Alert variant="success">{this.state.responseMsg}</Alert>
              ) : (
                ""
              )}
              <Form.Row>
                <Form.Group as={Col} controlId="firstName">
                  <Form.Label>First Name</Form.Label>
                  <Form.Control
                    required
                    type="text"
                    placeholder="Enter Your First Name"
                  />
                </Form.Group>

                <Form.Group as={Col} controlId="lastName">
                  <Form.Label>LastName</Form.Label>
                  <Form.Control
                    required
                    type="text"
                    placeholder="Enter Your Last Name"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col} controlId="email">
                  <Form.Label>Email address</Form.Label>
                  <Form.Control
                    required
                    type="email"
                    placeholder="Enter email"
                  />
                  <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                  </Form.Text>
                </Form.Group>
                <Form.Group as={Col} controlId="picture">
                  <Form.Label>Profile Picture</Form.Label>
                  <Form.Control
                    required
                    type="text"
                    placeholder="Enter your profile picture link's"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col} controlId="password">
                  <Form.Label>Password</Form.Label>
                  <Form.Control
                    required
                    type="password"
                    placeholder="Password"
                  />
                </Form.Group>
                <Form.Group as={Col} controlId="confirmPassword">
                  <Form.Label>Confirm Password</Form.Label>
                  <Form.Control
                    required
                    type="password"
                    placeholder="Password"
                  />
                  {this.state.errorPass ? (
                    <Form.Text className="error">
                      Passwords are not the same
                    </Form.Text>
                  ) : (
                    ""
                  )}
                </Form.Group>
              </Form.Row>

              <Button variant="success" type="submit">
                Submit
              </Button>
            </Form>
          </div>
        );
    }
}