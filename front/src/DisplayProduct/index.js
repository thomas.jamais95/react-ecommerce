import React from "react";
import Container from "react-bootstrap/Container"
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import { connect } from "react-redux"

import './style.css'

class DisplayProduct extends React.Component {
    constructor(props) {
      super(props);

      // eslint-disable-next-line no-unused-vars
      let state = {
        product: {},
      };
    }
  componentWillMount() {

    console.log(this);
    let pro = this.props.products.filter(
      (elem) => String(elem.id) === this.props.match.params.id
    );
    console.log(pro);
    this.setState({product: pro[0]})


  }

  render() {

    return (
      <Container className="product-container">
        <Row>
          <Col xs={8}>
            <img
              className="w-100"
              alt={this.state.product.name}
              src={
                this.state.product.picture ||
                "https://cdn.shopify.com/s/files/1/0484/7648/7837/products/outdoor-3681924_1920_1024x1024@2x.jpg?v=1600519245"
              }
            />
          </Col>
          <Col className="text-center">
            <h2>{this.state.product.name}</h2>
            <br />
            <h3>{this.state.product.price}€ </h3>
            <br />
            <br />
            <p className="text-justify">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras
              efficitur tellus urna, pretium pulvinar massa gravida at. Praesent
              interdum finibus lacus, in gravida arcu tristique sit amet. Donec
              ut rutrum justo. Vestibulum tristique sit amet mi sed mollis.
              Proin ut consectetur sapien, a mattis sem. Aenean congue, leo et
              cursus varius, orci erat pellentesque massa, sed cursus mauris
              ligula eu odio. Lorem ipsum dolor sit amet, consectetur adipiscing
              elit.
            </p>
            <Button variant="success" className="w-75">Add to Basket</Button>
          </Col>
        </Row>
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  products: state.product.products,
});

export default connect(mapStateToProps)(DisplayProduct);

        // <Card
        //   bg="light"
        //   text="dark"
        //   // style={{ width: "18rem" }}
        //   className="w-75"
        // >
        //   <Card.Header>{this.state.product.name}</Card.Header>
        //   <Card.Img
        //     variant="left"
        //     src={
        //       this.state.product.picture ||
        //       "https://cdn.shopify.com/s/files/1/0484/7648/7837/products/outdoor-3681924_1920_1024x1024@2x.jpg?v=1600519245"
        //     }
        //     className="w-50"
        //   />
        //   <Card.Body>
        //     <Card.Title> Description </Card.Title>
        //     <Card.Text>{this.state.product.shortDescription}</Card.Text>
        //   </Card.Body>
        // </Card>;
