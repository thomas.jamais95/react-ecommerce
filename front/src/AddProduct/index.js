import React from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Col from "react-bootstrap/Col";
import Alert from "react-bootstrap/Alert";
import axios from "axios";
import { connect } from "react-redux";
import { addProduct } from "../store/actions/products";

import './style.css'

class AddProduct extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      errorPass: false,
      showError: false,
      showSuccess: false,
      responseMsg: "",
    };
  }

  trySubmit = (event) => {
    event.preventDefault();
    // if (
    //   event.target.elements.password.value !==
    //   event.target.elements.confirmPassword.value
    // ) {
    //   this.setState({ errorPass: true });
    //   // this.state.errorPass = true
    // } else {
    this.setState({ errorPass: false });
    console.log(this.props);
    // this.state.errorPass = false
    let toSend = {
      name: event.target.elements.name.value,
      shortDescription: event.target.elements.shortdescription.value,
      category: event.target.elements.category.value,
      userId: this.props.id,
      picture: event.target.elements.picture.value,
      price: event.target.elements.price.value,
    };

    console.log("TOSEND", toSend);
    axios
      .post("http://localhost:3000/product/", toSend, {
        headers: { authorization: `Bearer ${localStorage.getItem("token")}` },
      })
      .then((response) => {
        if (response.status === 203) {
          this.setState({ responseMsg: response.data, showError: true });
        } else {
          this.setState({ responseMsg: response.data, showSuccess: true });
          this.props.addProduct(toSend);
        }
        console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
    console.log(toSend);
    // }
  };

  render() {
    return (
      <div className="addproduct-form-container">
        <Form
          className="align-items-center addproduct-form"
          width="50"
          onSubmit={this.trySubmit.bind(this)}
        >
          {this.state.showError ? (
            <Alert variant="danger">{this.state.responseMsg}</Alert>
          ) : (
            ""
          )}
          {this.state.showSuccess ? (
            <Alert variant="success">{this.state.responseMsg}</Alert>
          ) : (
            ""
          )}
          <Form.Row>
            <Form.Group as={Col} controlId="name">
              <Form.Label>Product Name</Form.Label>
              <Form.Control
                required
                type="text"
                placeholder="Enter Your First Name"
              />
            </Form.Group>

            <Form.Group as={Col} controlId="shortdescription">
              <Form.Label>Short Description</Form.Label>
              <Form.Control
                required
                type="text"
                placeholder="Enter A short description"
              />
            </Form.Group>
          </Form.Row>
          <Form.Row>
            <Form.Group as={Col} controlId="category">
              <Form.Label>Category</Form.Label>
              <Form.Control
                required
                type="text"
                placeholder="Enter a Category"
              />
            </Form.Group>
            <Form.Group as={Col} controlId="price">
              <Form.Label>Price</Form.Label>
              <Form.Control
                required
                type="text"
                placeholder="Price of the product"
              />
            </Form.Group>
          </Form.Row>
          <Form.Row>
            <Form.Group as={Col} controlId="picture">
              <Form.Label>Picture Link</Form.Label>
              <Form.Control
                required
                type="text"
                placeholder="Link to the picture"
              />
            </Form.Group>
          </Form.Row>

          <Button variant="success" type="submit">
            Submit
          </Button>
        </Form>
      </div>
    );
  }
}

const mapDispatchToProps = {
  addProduct,
};

const mapStateToProps = (state) => ({
  id: state.user.id,
  name: state.user.name,
  products: state.product.products,
});

export default connect(mapStateToProps, mapDispatchToProps)(AddProduct);
