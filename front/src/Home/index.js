import React from 'react'

export default class Home extends React.Component {

    render() {
        return (
            <div>
                <h1>Bonjour à vous {this.props.name || "Inconnus"}</h1>
            </div>
        )
    }
}