const user = (state = {}, action) => {
  switch (action.type) {
    case "ADD_JWT":
      return {
        ...state,
        jwt: action.jwt,
      };
    case "REMOVE_JWT":
      return {
        ...state,
        jwt: null,
      };
    case "SET_NAME":
      return {
        ...state,
        name: action.name,
      };
    case "SET_PICTURE":
      return {
        ...state,
        picture: action.picture,
      };
    case "SET_ID":
      return {
        ...state,
        id: action.id,
      };
    case "GET_JWT":
      return {
        ...state,
      };
    default:
      return state;
  }
};

export default user;
