export const addJwt = (jwt) => ({
  type: "ADD_JWT",
  jwt: jwt,
});

export const removeJwt = () => ({
    type: "REMOVE_JWT",
})

export const getJwt = () => ({
    type: "GET_JWT"
})

export const setName = (name) => ({
    type: "SET_NAME",
    name: name
})

export const setPicture = (picture) => ({
  type: "SET_PICTURE",
  picture: picture,
});

export const setId = (id) => ({
  type: "SET_ID",
  id: id,
});