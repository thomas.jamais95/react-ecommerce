export const addProducts = (products) => ({
  type: "ADD_PRODUCTS",
  products: products,
});

export const addProduct = (product) => ({
    type: "ADD_PRODUCT",
    product: product
})

// export const removeJwt = () => ({
//   type: "REMOVE_JWT",
// });

export const getProduct = (id) => ({
    type: "GET_PRODUCT",
    id: id
})

export const getProducts = () => ({
  type: "GET_PRODUCTS",
});
