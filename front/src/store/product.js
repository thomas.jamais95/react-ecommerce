
const initialProducts = {
    products: []
}

const product = (state = initialProducts, action) => {
  switch (action.type) {
    case "ADD_PRODUCTS":
      return {
        ...state,
        products: action.products,
      }
    case "ADD_PRODUCT":
      return {
        ...state,
        products: [...state.products, action.product]
      }
    case "GET_PRODUCTS":
      return {
        ...state,

      }
    case "GET_JWT":
        return {
            ...state
        }
    default:
      return state;
  }
};

export default product;
