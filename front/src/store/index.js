import { combineReducers } from "redux";

import user from "./user";
import product from "./product";
// import visibilityFilter from "./visibilityFilter";

export default combineReducers({
  user,
  product
//   visibilityFilter,
});
