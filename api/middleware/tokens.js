const mysql = require("mysql2");
const jwt = require("jsonwebtoken");
const { execute } = require("../database/db");

module.exports = {
  verifyEmail: async (req, res, next) => {
    const query = mysql.format(
      `
        SELECT * FROM ?? WHERE ?? = ? ;
      `,
      ["users", "email", req.body.email]
    );
    let response = await execute(query);
    //console.log(response.length);
    if (response.length === 0) next();
    else {
      res.status(203).json({
        message: `user ${req.body.name} already exist`,
      });
    }
  },

  verifyToken: async (req, res, next) => {
    try {
      const token = req.headers["authorization"];

      //cut the 'Bearer ' of the token.
      const clean_token = token.slice(7);
      console.log(token);
      console.log(clean_token);

      const decode = await jwt.verify(clean_token, "123456789");
      console.log(decode, req.body.userId)
      if (decode.id != req.body.userId) {
          res.status(203).json({message: "You are not allowed to post Products with this ID"})
      } else {
          next()
      }
    } catch (err) {
      console.log(err);
      res
        .status(501)
        .json({
          message:
            "Sorry there is a problem in the verification of your token.",
        });
    }

    // next();
  },
};

