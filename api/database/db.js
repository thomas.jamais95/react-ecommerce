const mysql = require("mysql2")

    try {
        const pool = mysql.createPool({
          host: "localhost",
          user: "root",
          password: "root",
          database: "ecom",
          waitForConnections: true
        });
        const execute = async (whatToExec) => {
          let [rows] =  await pool.promise().query(whatToExec);
          return rows;
        };

        const normalSelect = (table) => {
            return `SELECT * from ${table}`
        }
        
        module.exports = {
            execute,
            normalSelect
        }
    } catch (error) {
        console.error("THERE IS AN ERROR WHILE CREATING CONNECTION");
        console.log(error);
        throw error
    }