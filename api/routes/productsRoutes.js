const route = require("express").Router();
const { execute, normalSelect } = require("../database/db");
const { verifyToken } = require("../middleware/tokens")

route
  .get("/", async (req, res) => {
    try {
        res.json(await execute(normalSelect("products")));
    } catch (error) {
        res.status(203).send(`ERROR OCCURED : ${error}`);
    }
  })
  .post("/", verifyToken, async (req, res) => {
    try {
        if (!req.body.name) throw "NO NAME";
        if (!req.body.category) throw "NO CATEGORY";
        if (!req.body.picture) throw "NO PICTURE";
        if (!req.body.price) throw "NO PRICE";
        if (!req.body.shortDescription) throw "NO SHORT DESCRIPTION";
        if (!req.body.userId) throw "NO USER ID";

        let toExec = `INSERT INTO products (name, shortDescription, category, userId, picture, price) VALUES ('${req.body.name}', '${req.body.shortDescription}', '${req.body.category}', '${req.body.userId}', '${req.body.picture}', '${req.body.price}')`;
        let results = await execute(toExec);
        console.log(results);
        res.send("INSERTED");
    } catch (error) {
      res.status(203).send(`ERROR OCCURED : ${error}`);
    }
  });
module.exports = route