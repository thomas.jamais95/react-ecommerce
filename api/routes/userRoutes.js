const route = require("express").Router()
const bcrypt = require("bcrypt")
const { execute, normalSelect } = require("../database/db")
const jwt = require("jsonwebtoken")
const { verifyEmail } = require("../middleware/tokens")

route.get('/', async (req, res) => {
    try {
      res.json(await execute(normalSelect("users")));
    } catch (error) {
      res.status(203).send(`ERROR OCCURED : ${error}`);
    }
})
.post('/sign-up', verifyEmail, (req, res) => {
    try {
        if (!req.body.firstName) throw "NO FIRST NAME";
        if (!req.body.lastName) throw "NO LAST NAME";
        if (!req.body.email) throw "NO EMAIL"
        if (!req.body.password) throw "NO PASSWORD";
        if (!req.body.picture) throw "NO PICTURE";

        bcrypt.hash(req.body.password, 10, async (error, encrypted) => {
            let toExec = `INSERT INTO users (firstname, lastname, password, email, picture) VALUES ('${req.body.firstName}', '${req.body.lastName}', '${encrypted}', '${req.body.email}', '${req.body.picture}')`
            let results = await execute(toExec)
            console.log(results);
            res.send("INSERTED")
        })
    } catch (error) {
        res.status(203).send(`ERROR OCCURED : ${error}`);
    }
})
.post('/sign-in', async (req, res) => {
    try {
        if (!req.body.email) throw "NO EMAIL";
        if (!req.body.password) throw "NO PASSWORD";
        let toExec = `SELECT * FROM users WHERE email = '${req.body.email}'`;
        let results = await execute(toExec)
        if (results.length) {
            bcrypt.compare(req.body.password, results[0].password, (error, isGood) => {
                if (error) throw error
                if (!isGood) {
                    res.status(203).send(`ERROR OCCURED : BAD PASSWORD`)
                } else {
                    let token = jwt.sign({email: req.body.email, id: results[0].id, name: results[0].name, picture: results[0].picture}, "123456789")
                    res.json({token: token, message: "Connected"})
                }
            })
        } else {
            throw "ACCOUNT DOESN'T EXIST"
        }
    } catch (error) {
        res.status(203).send(`ERROR OCCURED : ${error}`);
    }
})

module.exports = route