const express = require('express')
const cors = require('cors')

const userRoutes = require("./routes/userRoutes");
const productsRoutes = require("./routes/productsRoutes");

const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("./swagger.json");

// app.use("/api/v1", router);


const api = express()

api.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
api.use(cors())
api.use(express.json())
api.use(express.urlencoded({extended: true}))

api.use("/user", userRoutes);
api.use("/product", productsRoutes);

api.listen(3000, () => {
    console.log("Connected to localhost:3000/");
})