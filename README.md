![](https://img.shields.io/badge/made%20with-javaScript-yellow?logo=javaScript).
![](https://img.shields.io/badge/made%20with-node.js-success?logo=node.js).
![](https://img.shields.io/badge/made%20with-react.js-blue?logo=react).
![](https://img.shields.io/badge/made%20with-redux-blueviolet?logo=redux).
![](https://img.shields.io/badge/made%20with-react_Bootstrap-blueviolet?logo=Bootstrap).
![](https://img.shields.io/badge/made%20with-mysql-blue?logo=mysql).
![](https://img.shields.io/badge/made%20with-jsonwebtokens-orange?logo=jwt).

![Hits](https://hitcounter.pythonanywhere.com/count/tag.svg?url=https%3A%2F%2Fgitlab.com%2Fthomas.jamais95%2Freact-ecommerce)


<img src="https://img.shields.io/badge/react.router.dom-blueviolet.svg" alt="react-router-dom">.
<img src="https://img.shields.io/badge/jwt-success" alt="jwt">.
<img src="https://img.shields.io/badge/bcrypt-red" alt="axios">.
<img src="https://img.shields.io/badge/Express-succes.svg" alt="Express">. 
<br>


# React Ecommerce
> This is a simple but full React Ecommerce Platforme

## Prerequisites
* NodeJS
* MySQL8
* Nodemon
    - Linux and Mac users run : 
        > ```sh
        > sudo npm install -g nodemon
        > ```
    - Windows users run :
        > ```sh
        > sudo npm install -g nodemon
        > ```

## Installation

### API
- `cd api`

- `npm install`

- `npm start`

### Front-End
- `cd front`

- `npm install`

- `npm start`

## API Endpoints

- After started API go to [Swagger View ](http://localhost:3000/api-docs/ "Swagger Doc")

### Screenshot

<img width="800" alt="Swagger" src="https://i.ibb.co/mNt8zWM/swagger.jpg"> 



## Contact	
- [![LinkedIn][linkedin-shield]][linkedin-url] 	
- thomas.jamais95@gmail.com






<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=flat-square&logo=linkedin&colorB=555
[linkedin-url]: https://www.linkedin.com/in/thomas-jamais/









